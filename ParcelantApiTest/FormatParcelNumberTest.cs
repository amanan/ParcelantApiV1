﻿using Microsoft.VisualBasic.CompilerServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParcelantApiV1.BusinessLogic.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParcelantApiTest
{
    [TestClass]
    public class FormatParcelNumberTest
    {
        [TestMethod]
        public void FormatNumberLessThan100_ShouldReturnThreeCharString()
        {
            var Formated = FormatParcelNumber.Format(12);
            Assert.AreEqual("012", Formated);
        }

        [TestMethod]
        public void FormartNumberGreaterThan99_shouldReturnWithOutLeadingZero()
        {
            var formated = FormatParcelNumber.Format(2100);
            Assert.AreEqual("2100", formated);
        }
    }
}
