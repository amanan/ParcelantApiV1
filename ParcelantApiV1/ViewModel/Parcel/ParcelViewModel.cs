﻿using ParcelantApiV1.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.ViewModel.Parcel
{
    public class ParcelViewModel
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int ClientBranchId { get; set; }
        public string ClientBranchName { get; set; }
        public int AppUserId { get; set; }
        public string AppUserName { get; set; }
        public string ParcelNumber { get; set; }
        public string SenderName { get; set; }
        public string SenderPhone { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverPhone { get; set; }
        public int RoutesId { get; set; }
        public string RouteName { get; set; }
        public int CollectionPointId { get; set; }
        public string CollectionPointName { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; } 
    }

   public class ParcelToDispatchViewModel:ParcelViewModel
   {
        public int TotalItems { get; set; }
        public int TotalItemsDispatch { get; set; }
        public int ParcelStatusDetailId { get; set; }
        public string ParcelStatusDetailName { get; set; }

    }

    public class ParcelToReceiveViewModel : ParcelViewModel
    {
        public int TotalItems { get; set; }
        public int TotalItemsReceive { get; set; }
        public int ParcelStatusDetailId { get; set; }
        public string ParcelStatusDetailName { get; set; }

    }
}
