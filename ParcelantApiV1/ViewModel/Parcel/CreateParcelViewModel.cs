﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.ViewModel.Parcel
{
    public class CreateParcelViewModel
    {
        public int ClientId { get; set; }
        public int ClientBranchId { get; set; }
        public int AppUserId { get; set; }
        public int SenderId { get; set; }
        public int ReceiverId { get; set; }
        public int DestinationId { get; set; }
        public int CollectionPointId { get; set; }
        
    }
}
