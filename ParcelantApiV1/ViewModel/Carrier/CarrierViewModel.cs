﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.ViewModel.Carrier
{
    public class CarrierViewModel
    {
    public int Id { get; set; }
    public string Name { get; set; }
    public string Code { get; set; }
  }
}
