﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.ViewModel.ParcelItem
{
    public class CreateParcelItemViewModel
    {
        public int ClientId { get; set; }
        public int ParcelId { get; set; }
        public int AppUserId { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public decimal Amount { get; set; }
    }
}
