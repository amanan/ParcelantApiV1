﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.ViewModel.ClientAdmin
{
    public class ClientAdminViewModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Phone Number")]
        public string Phone { get; set; }
        [Display(Name ="County")]
        public String County { get; set; }
        [Display(Name ="Client")]
        public string Client { get; set; }
        [Display(Name ="Created on")]
        public DateTime CreatedAt { get; set; }
        [Display(Name ="Last Modified" )]
        public DateTime UpdatedAt { get; set; }
    }

    public class ClientAdminIndexViewModel
    {
        public ClientAdminViewModel ClientAdminViewModel { get; set; }
        public IEnumerable<ClientAdminViewModel> ListClientAdminViewModel { get; set; }
      
    }
}
