﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AspNet.Security.OpenIdConnect.Extensions;
using AspNet.Security.OpenIdConnect.Primitives;
using AspNet.Security.OpenIdConnect.Server;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using ParcelantApiV1.Businesslogic;
using ParcelantApiV1.Businesslogic.Domain.Interfaces;
using ParcelantApiV1.ViewModel.AppUser;

namespace ParcelantApiV1.Controllers
{
    [Produces("application/json")]
    [Route("api/ApiAuth")]
    
    public class ApiAuthController : Controller
    {
        private IMapper _mapper;
        private IUnitOfWork _unitOfWork;
        private IAppUserAuthService _appUserAuthService;

     
        public ApiAuthController(
            IMapper mapper, 
            IUnitOfWork unitOfWork, 
            IAppUserAuthService appUserAuthService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _appUserAuthService = appUserAuthService;
        }
        [HttpPost("~/connect/token")]
        public IActionResult Exchange(OpenIdConnectRequest request)
        {
            if (request.IsPasswordGrantType())
            {
                if (_appUserAuthService.IsValidAccount(request.Username, request.Password))
                {
                    // Create a new ClaimsIdentity holding the user identity.
                    var identity = new ClaimsIdentity(
                        OpenIdConnectServerDefaults.AuthenticationScheme,
                        OpenIdConnectConstants.Claims.Name,
                        OpenIdConnectConstants.Claims.Role);

                    // Add a "sub" claim containing the user identifier, and attach
                    // the "access_token" destination to allow OpenIddict to store it
                    // in the access token, so it can be retrieved from your controllers.
                    identity.AddClaim(OpenIdConnectConstants.Claims.Subject,
                        "ClientApp",
                        OpenIdConnectConstants.Destinations.AccessToken);

                    identity.AddClaim(OpenIdConnectConstants.Claims.Name, request.Username,
                        OpenIdConnectConstants.Destinations.AccessToken);

                    // ... add other claims, if necessary.

                    var principal = new ClaimsPrincipal(identity);

                    // Ask OpenIddict to generate a new token and return an OAuth2 token response.
                    return SignIn(principal, OpenIdConnectServerDefaults.AuthenticationScheme);
                }
                else
                {
                   return BadRequest( new OpenIdConnectResponse{
                        Error = OpenIdConnectConstants.Errors.UnauthorizedClient,
                        ErrorDescription = "Invalid Username or Password"
                    });
                }

            }
            else
            {
                return BadRequest(new OpenIdConnectResponse
                {
                    Error = OpenIdConnectConstants.Errors.InvalidGrant,
                    ErrorDescription = "Unsupported Grant Type"
                });
            }
        }
    }
}