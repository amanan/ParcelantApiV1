﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParcelantApiV1.Businesslogic;
using ParcelantApiV1.BusinessLogic.Domain;
using ParcelantApiV1.BusinessLogic.Domain.Interfaces;
using ParcelantApiV1.Util.RestResponse;
using ParcelantApiV1.ViewModel.UpdateParcelStatus;

namespace ParcelantApiV1.Controllers
{
    [Produces("application/json")]
    [Route("api/Collection")]
    public class CollectionController : Controller
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        private IParcelStatusManager _parcelStatusManager;
        public CollectionController(IUnitOfWork unitOfWork, IMapper mapper, IParcelStatusManager parcelStatusManager)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _parcelStatusManager = parcelStatusManager;
        }
        // POST: api/Collection
        [HttpPost]
        [Authorize]
        public IActionResult Post([FromBody]UpdateParcelStatusViewModel updateParcelStatusViewModel, int clientId, int branchId)
        {
            var response = new SingleDataResponse<bool>();
            if (updateParcelStatusViewModel == null)
            {
                response.Data = false;
                response.Error = true;
                response.ErrorMessage = "No data passed";
                response.Status = HttpStatusCode.BadRequest;
                return response.SingleResponse();
            }

            var client = _unitOfWork.Clients.Get(clientId);
            if (client == null)
            {
                response.Data = false;
                response.Error = true;
                response.ErrorMessage = "Unauthorized user";
                response.Status = HttpStatusCode.BadRequest;
                return response.SingleResponse();

            }

            var clientBranch = _unitOfWork.ClientBranch.GetClientBranch(clientId, branchId);
            if (clientBranch == null)
            {
                response.Data = false;
                response.Error = true;
                response.ErrorMessage = "Unauthorized user";
                response.Status = HttpStatusCode.BadRequest;
                return response.SingleResponse();

            }

            var clientAppUser = _unitOfWork.ClientAppUser.GetAppUserInfo(updateParcelStatusViewModel.AppUserId);
            if (clientAppUser.Client.Id != clientId && clientAppUser.ClientBranch.Id != branchId)
            {
                response.Data = false;
                response.Error = true;
                response.ErrorMessage = "Unauthorized user";
                response.Status = HttpStatusCode.BadRequest;
                return response.SingleResponse();

            }
            var intendendParcelStatus = _unitOfWork.ParcelStatus
                               .GetParcelStatusByName(ParcelStatusTrack.COLLECTED_ALL_ITEMS);
            var fallbackParcelStatus = _unitOfWork.ParcelStatus
                               .GetParcelStatusByName(ParcelStatusTrack.COLLECTED_SOME_ITEMS);

            var fromParcelItemStatus = _unitOfWork.ParcelItemStatus.GetParcelItemStatusByName(ParcelStatusTrack.ITEM_RECEIVED_AT_DESTINATION);

            var appUser = clientAppUser.AppUser;

            var parcelItemStatus = _unitOfWork.ParcelItemStatus.GetParcelItemStatusByName(ParcelStatusTrack.ITEM_COLLECTED);

            var hasNext = true;

            try
            {
                _parcelStatusManager.UpdateParcelStatus(client, clientBranch, appUser, updateParcelStatusViewModel,
                                                        intendendParcelStatus, fallbackParcelStatus, fromParcelItemStatus, parcelItemStatus, hasNext);

                response.Error = false;
                response.Status = HttpStatusCode.OK;
                response.Data = true;

            }
            catch (Exception e)
            {
                response.Data = false;
                response.Error = true;
                response.ErrorMessage = "Unable to update parcel status";
                response.Status = HttpStatusCode.BadRequest;
            }

            return response.SingleResponse();
        }
        
        // PUT: api/Collection/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
