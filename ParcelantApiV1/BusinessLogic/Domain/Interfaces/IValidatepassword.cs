﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.Businesslogic.Domain.Interfaces
{
    public interface IValidatepassword
    {
        bool validate(string plainText, string hashed);

    }
}
