﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.BusinessLogic.Domain
{
    public static class ParcelStatusTrack
    {
        private static string _recorded  = "RECORDED";
        private static string _dFS =  "DFS";
        private static string _rAD = "RAD";
        private static string _rFC = "RFC";
        private static string _cAI = "CAI";
        private static string _cSI = "CSI";
        private static string _rADD = "RADD";
        private static string _iDFS = "IDFS";
        private static string _dSI = "DSI";
        private static string _iCLTD = "ICLTD";
        private static string _iRAD = "IRAD";
        private static string _rAIAD = "RAIAD";
        private static string _rSIAD = "RSIAD";

        public static string RECORDED { get => _recorded; private set => _recorded = value; }
        [Description("Dispatched from the source")]
        public static string DISPATCHED_FROM_THE_SOURCE { get => _dFS; private set => _dFS = value; }
        [Description("Received At the Destination")]
        public static string RECEIVED_AT_DESTINATION { get => _rAD; private set => _rAD = value; }
        [Description("Ready for collection")]
        public static string READY_FOR_COLLECTION { get => _rFC; private set => _rFC = value; }
        public static string COLLECTED_ALL_ITEMS { get => _cAI; private set => _cAI = value; }
        public static string COLLECTED_SOME_ITEMS { get => _cSI; private set => _cSI = value; }
        public static string RECEIVED_AT_DIFFERENT_DESTINATION { get => _rADD; private set => _rADD = value; }
        public static string ITEM_DISPATCHED_FROM_SOURCE { get => _iDFS; private set => _iDFS = value; }
        public static string DISPATCHED_SOME_ITEMS { get => _dSI; private set => _dSI = value ; }
        public static string ITEM_COLLECTED { get => _iCLTD; private set => _iCLTD = value; }
        public static string ITEM_RECEIVED_AT_DESTINATION { get => _iRAD; private set=> _iRAD = value; }
        public static string RECEIVED_ALL_ITEMS_AT_DESTINATION { get=> _rAIAD; private set => _rAIAD = value; }
        public static string RECEIVED_SOME_ITEMS_AT_DESTINATION { get => _rSIAD;private set => _rSIAD = value; }
    }



}
