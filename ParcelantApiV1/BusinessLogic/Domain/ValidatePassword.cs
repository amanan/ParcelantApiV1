﻿using ParcelantApiV1.Businesslogic.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.Businesslogic.Domain
{

    public class ValidatePassword : IValidatepassword
    {
        public bool validate(string plainText, string hashed)
        {
            return Util.PasswordCrypto.VerifyHashedPassword(hashed, plainText);
        }
    }
}
