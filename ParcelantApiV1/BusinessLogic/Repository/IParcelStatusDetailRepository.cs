﻿using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repositoy;
using ParcelantApiV1.ViewModel.UpdateParcelStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.BusinessLogic.Repository
{
    public interface IParcelStatusDetailRepository:IRepository<ParcelStatusDetail>
    {
        IEnumerable<ParcelStatusDetail> GetAllParcelStatus(int clientId, int branchId, int intendedParcelStatus, int fallbackParcelStatus);
        bool CheckParcelHasStatus(int clientId, int parcelId, int parcelStatusId);
    }
}
