﻿using Microsoft.AspNetCore.Routing;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.Businesslogic.Repository
{
    public interface IRouteRepository: IRepository<Routes>
    {
        IEnumerable<Routes> GetClientRoutes(int clientId);
        Routes GetClientRoute(int clientId,int routeId);
        IEnumerable<RouteStop> GetClientBranchRoutes(int clientId, int branchId);
        Routes GetRouteByOrginAndDestination(int clientid, int originid, int destinationid);
    }
}
