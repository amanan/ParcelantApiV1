﻿using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.BusinessLogic.Repository
{
    public interface IParcelRepository:IRepository<Parcel>
    {
        Parcel GetParcelDetail(int clientId, int parcelId);
        Parcel GetParcelByParcelNumber(int clientId, string parcelNumber);
        IEnumerable<Parcel> GetParcels(int clientId);
        Parcel GetParcelByClientBranch(int clientId, int branchId, int parcelId);
    }

 
}
