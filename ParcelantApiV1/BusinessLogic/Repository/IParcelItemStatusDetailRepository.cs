﻿using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.BusinessLogic.Repository
{
    public interface IParcelItemStatusDetailRepository:IRepository<ParcelItemStatusDetail>
    {
        ParcelItemStatusDetail GetParcelItemStatusDetail(int parcelId,int parcelItemId,int itemStatusId);
       IEnumerable<ParcelItemStatusDetail> GetParcelItemStatusDetail(int parcelId, int itemStatusId);
    }
}   
