﻿using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repositoy;
using ParcelantApiV1.ViewModel.AppUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.Businesslogic.Repository
{
    public interface IAppUserRepository:IRepository<AppUser>
    {
        AppUser GetAppUserByUsername(string username);
    }
}
