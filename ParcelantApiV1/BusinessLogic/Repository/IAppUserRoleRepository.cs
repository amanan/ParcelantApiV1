﻿using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.Businesslogic.Repository
{
    public interface IAppUserRoleRepository : IRepository<AppUserRole>
    {
        AppUserRole GetAppUserRoleByCode(int clientId, int appUserId, string code);
        IEnumerable<AppUserRole> GetAppUsersRoleByCode(int clientid, string roleCode);
  }

}
