﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspNet.Security.OAuth.Validation;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ParcelantApiV1.Businesslogic;
using ParcelantApiV1.Businesslogic.Domain;
using ParcelantApiV1.Businesslogic.Domain.Interfaces;
using ParcelantApiV1.BusinessLogic.Domain;
using ParcelantApiV1.BusinessLogic.Domain.Interfaces;
using ParcelantApiV1.DAL;
using ParcelantApiV1.ViewModel.AppUser;
using Swashbuckle.AspNetCore.Swagger;

namespace ParcelantApiV1
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddDbContext<ParcelantApiV1Context>(options =>
      {
        options.UseSqlServer(Configuration.GetConnectionString("ParcelantApiV1"));
        // Register the entity sets needed by OpenIddict.
        // Note: use the generic overload if you need
        // to replace the default OpenIddict entities.
        options.UseOpenIddict();
      });


      services.AddTransient<IUnitOfWork, UnitOfWork>();
      services.AddTransient<IAuthPortalUser, AuthPortalUser>();
      services.AddTransient<IValidatepassword, ValidatePassword>();
      services.AddTransient<IAuthClaimsPrincipal, AuthClaimsPrincipal>();
      services.AddTransient<IAppUserAuthService, AppUserAuthService>();

      services.AddTransient<IParcelStatusManager, ParcelStatusManager>();
      services.AddTransient<IDispatchManager, DispatchManager>();
      services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

      services.AddOpenIddict(options =>
      {
              // Register the Entity Framework stores.
              options.AddEntityFrameworkCoreStores<ParcelantApiV1Context>();

              // Register the ASP.NET Core MVC binder used by OpenIddict.
              // Note: if you don't call this method, you won't be able to
              // bind OpenIdConnectRequest or OpenIdConnectResponse parameters.
              options.AddMvcBinders();

              // Enable the token endpoint.
              options.EnableTokenEndpoint("/connect/token");

              // Enable the password flow.
              options.AllowPasswordFlow();

              // During development, you can disable the HTTPS requirement.
              options.DisableHttpsRequirement();
      });

      // Register the validation handler, that is used to decrypt the tokens.
      services.AddAuthentication(options =>
      {
        options.DefaultScheme = OAuthValidationDefaults.AuthenticationScheme;
      }).AddOAuthValidation();

      //services.AddIdentity<AppUserInfoViewModel, IdentityRole>();

      services.AddMvc();
      services.AddAutoMapper();

      services.AddSwaggerGen(swagger =>
      {
        swagger.SwaggerDoc("v1", new Info
        {
          Title = "Parcelant API",
          License = new License
          {
            Name = "Parcelent Client Integration",
            Url = "",
          },
          Description = "API for Mobile App for transportation companies",
          Contact = new Contact
          {
            Email = "admin@parcelant.com",
            Name = "Abdulmanan Ahmed",
            Url = "https://parcelant.com"
          },
          Version = "V1",

        });

        swagger.AddSecurityDefinition("Bearer", new OAuth2Scheme
        {
          TokenUrl = "/connect/token",
          Description = "OAuth authentication",
          Flow = "password",
          Type = "oauth2",
        });
      });
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      app.UseCors(option =>
      {
        option.WithOrigins("http://localhost:8100")
          .AllowAnyHeader()
          .AllowAnyMethod();
      });
      app.UseAuthentication();

      app.UseMvcWithDefaultRoute();

      app.UseSwagger();
      app.UseSwaggerUI(swaggerUI =>
      {
        swaggerUI.SwaggerEndpoint("/swagger/v1/swagger.json", "Parcelant API");
      });
    }
  }
}
