﻿using Microsoft.EntityFrameworkCore;
using ParcelantApiV1.DAL.Model;
using ParcelantApiV1.DAL.Models;

namespace ParcelantApiV1.DAL

{
    public class ParcelantApiV1Context : DbContext
    {
        public ParcelantApiV1Context(DbContextOptions<ParcelantApiV1Context> options)
             : base(options) { }
        public ParcelantApiV1Context() { }
        public DbSet<Client> Client { get; set; }
        public DbSet<ClientBranch> ClientBranch { get; set; }
       public DbSet<County> County { get; set; }
    
        public DbSet<AppUser> AppUser { get; set; }
        public DbSet<AppUserRole> AppUserRole { get; set; }
        public DbSet<AppRole> AppRole { get; set; }
        public DbSet<ClientAppUser> ClientAppUser { get; set; }

        public DbSet<Carrier> Carrier { get; set; }

        public DbSet<RouteStop> RouteStop { get; set; }
        public DbSet<Routes> Routes { get; set; }
        public DbSet<ClientBranchRouteStop> ClientBranchRouteStop { get; set; }
        public DbSet<Parcel> Parcel { get; set; }
        public DbSet<ParcelItem> ParcelItem { get; set; }
        public DbSet<ParcelItemCarrier> ParcelItemCarrier { get; set; }
        public DbSet<ParcelStatus> ParcelStatus { get; set; }
        public DbSet<ParcelItemStatus> ParcelItemStatus { get; set; }
        public DbSet<ParcelStatusDetail> ParcelStatusDetail { get; set; }
        public DbSet<ParcelItemStatusDetail> ParcelItemStatusDetail { get; set; }
        public DbSet<ParcelCurrentStatus> ParcelCurrentStatus { get; set; }
        public DbSet<ParcelItemCurrentStatus> ParcelItemCurrentStatus { get; set; }
        public DbSet<ClientParcelNumberCounter> ClientParcelNumberCounter { get; set; }
        public DbSet<CollectionPoint> CollectionPoint { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<ClientDispatchCounter> ClientDispatchCounter { get; set; }
        public DbSet<DispatchGroup> DispatchGroup { get; set; }
        public DbSet<DispatchedParcels> DispatchedParcels { get; set; }
    }
}