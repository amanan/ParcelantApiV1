﻿using Microsoft.EntityFrameworkCore;
using ParcelantApiV1.BusinessLogic.Repository;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Repository
{
    public class ParcelItemStatusDetailRepository : Repository<ParcelItemStatusDetail>, IParcelItemStatusDetailRepository
    {
        public ParcelItemStatusDetailRepository(ParcelantApiV1Context context) : base(context)
        {
        }

        public ParcelItemStatusDetail GetParcelItemStatusDetail(int parcelId, int parcelItemId, int itemStatusId)
        {
            return Context.ParcelItemStatusDetail
                .Where(x => x.Parcel.Id == parcelId && x.ParcelItem.Id == parcelItemId && x.ParcelItemStatus.Id == itemStatusId)
                .FirstOrDefault();
        }

        public IEnumerable<ParcelItemStatusDetail> GetParcelItemStatusDetail(int parcelId, int itemStatusId)
        {
          return  Context.ParcelItemStatusDetail
                    .Include(x => x.AppUser)
                    .Include(x => x.Parcel)
                    .Include(x => x.ParcelItem)
                    .Include(x => x.ParcelItemStatus)
                    .Where(x => x.Parcel.Id == parcelId && x.ParcelItemStatus.Id == itemStatusId)
                    .ToList();
        }
    }
}
