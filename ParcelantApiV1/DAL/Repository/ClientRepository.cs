﻿using Microsoft.EntityFrameworkCore;
using ParcelantApiV1.Businesslogic.Repository;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Repositoy
{
    public class ClientRepository : Repository<Client>, IClientRepository
    {
        public ClientRepository(ParcelantApiV1Context context) : base(context)
        {
        }

        
        public IEnumerable<ClientViewModel> getClients()
        {
            return Context.Client.Include(x=>x.County).ToList().Select(xy=>
                       new ClientViewModel
                       {
                           Id = xy.Id,
                           Name = xy.Name,
                           Email = xy.Email,
                           Phone = xy.Phone,
                           Active = xy.Active,
                           County = xy.County.Name,
                           Address = xy.Address,
                           CreatedAt = xy.CreatedAt,
                           UpdatedAt = xy.UpdatedAt
                       }).ToList();
        }
    }
}
