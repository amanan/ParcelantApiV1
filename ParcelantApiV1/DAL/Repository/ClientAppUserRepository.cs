﻿using Microsoft.EntityFrameworkCore;
using ParcelantApiV1.Businesslogic.Repository;
using ParcelantApiV1.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Repositoy
{
    public class ClientAppUserRepository : Repository<ClientAppUser>, IClientAppUserRepository
    {
        public ClientAppUserRepository(ParcelantApiV1Context context) : base(context)
        {
        }

        public ClientAppUser GetAppUserInfo(int appUserId)
        {
          return  Context.ClientAppUser
                            .Include(x => x.AppUser)
                            .Include(x => x.Client)
                            .Include(x => x.ClientBranch)
                            .Where(x => x.AppUser.Id == appUserId).FirstOrDefault();
        }

        public IEnumerable<ClientAppUser> getClientAppUsers(int clientId)
        {
           return Context.ClientAppUser
                .Include(x => x.Client)
                .Include(x => x.ClientBranch)
                .Where(x => x.Client.Id == clientId).ToList();
        }
    }
}
