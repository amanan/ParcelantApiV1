﻿using ParcelantApiV1.Businesslogic.Repository;
using ParcelantApiV1.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Repositoy
{
    public class ParcelStatusRepository : Repository<ParcelStatus>, IParcelStatusRepository
    {
        public ParcelStatusRepository(ParcelantApiV1Context context) : base(context)
        {
        }

        public ParcelStatus GetParcelStatusByName(string statusName)
        {
            return Context.ParcelStatus.Where(x => x.Name == statusName.ToUpper()).FirstOrDefault();
        }
    }
}
