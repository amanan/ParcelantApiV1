﻿using ParcelantApiV1.BusinessLogic.Repository;
using ParcelantApiV1.DAL.Model;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Repository
{
    public class ClientParcelNumberCounterRepository : Repository<ClientParcelNumberCounter>, IClientParcelNumberCounterRepository
    {
        public ClientParcelNumberCounterRepository(ParcelantApiV1Context context) : base(context)
        {
        }

        public ClientParcelNumberCounter GetClientCurrentCounter(int clientId)
        {
            return Context.ClientParcelNumberCounter.Where(x => x.Client.Id == clientId).FirstOrDefault();
        }
    }
}
