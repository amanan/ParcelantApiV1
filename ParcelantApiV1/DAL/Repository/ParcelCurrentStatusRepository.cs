﻿using Microsoft.EntityFrameworkCore;
using ParcelantApiV1.BusinessLogic.Repository;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Repository
{
    public class ParcelCurrentStatusRepository : Repository<ParcelCurrentStatus>, IParcelCurrentStatusRepository
    {
        public ParcelCurrentStatusRepository(ParcelantApiV1Context context) : base(context)
        {
        }

        public IEnumerable<ParcelCurrentStatus> GetAllParcelCurrentStatus(int clientId, 
                                                                           int branchId, 
                                                                           int intendedParcelStatus, 
                                                                           int fallbackParcelStatus)
        {
            /*
             * mainly used to get parcels for dispatch
             * 
             */
            return Context.ParcelCurrentStatus
                     .Include(x => x.AppUser)
                     .Include(x => x.Parcel).ThenInclude(x => x.Sender)
                     .Include(x => x.Parcel).ThenInclude(x => x.Receiver)
                     .Include(x => x.Parcel.Routes).ThenInclude(x => x.Origin)
                     .Include(x => x.Parcel.Routes).ThenInclude(x => x.Destination)
                     .Include(x => x.Parcel.CollectionPoint)
                     .Include(x => x.ParcelStatus)
                     .Include(x => x.Client)
                     .Include(x => x.Parcel.ClientBranch)
                     .Where(x => x.ParcelStatus.Id == intendedParcelStatus
                         || x.ParcelStatus.Id == fallbackParcelStatus
                         && x.Parcel.Client.Id == clientId
                         && x.Parcel.ClientBranch.Id == branchId)
                         .ToList();

        }

        public IEnumerable<ParcelCurrentStatus> GetAllParcelsToReceive(int clientId,
                                                          int branchId,
                                                          int intendedParcelStatus, 
                                                          int fallbackParcelStatus, 
                                                          int destinationId)
        {

            return Context.ParcelCurrentStatus
                    .Include(x => x.AppUser)
                    .Include(x => x.Parcel).ThenInclude(x => x.Sender)
                    .Include(x => x.Parcel).ThenInclude(x => x.Receiver)
                    .Include(x => x.Parcel.Routes).ThenInclude(x => x.Origin)
                    .Include(x => x.Parcel.Routes).ThenInclude(x => x.Destination)
                    .Include(x => x.Parcel.CollectionPoint)
                    .Include(x => x.ParcelStatus)
                    .Include(x => x.Client)
                    .Include(x => x.Parcel.ClientBranch)
                    .Where(x => x.ParcelStatus.Id == intendedParcelStatus
                        || x.ParcelStatus.Id == fallbackParcelStatus
                        && x.Parcel.Client.Id == clientId
                        && x.Parcel.Routes.Destination.Id == destinationId)
                        .Join(Context.ClientBranchRouteStop.Where(x=>x.ClientBranch.Id ==branchId),
                               parcelStatus => parcelStatus.Parcel.Routes.Destination.Id,
                               branchStop => branchStop.RouteStop.Id,
                               (parcelStatus, branchStop) => new { ParcelCurrentStatus = parcelStatus }
                               ).Select(x=>x.ParcelCurrentStatus)
                        .ToList();



        }

        public ParcelCurrentStatus GetCurentStatus(int clientId, int parcelId)
        {
            return Context.ParcelCurrentStatus
                .Where(x => x.Parcel.Id == parcelId && x.Client.Id == clientId).FirstOrDefault();
        }
    }
}
