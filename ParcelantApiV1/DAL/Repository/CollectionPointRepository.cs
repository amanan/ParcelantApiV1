﻿using Microsoft.EntityFrameworkCore;
using ParcelantApiV1.BusinessLogic.Repository;
using ParcelantApiV1.DAL.Model;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Repository
{
    public class CollectionPointRepository : Repository<CollectionPoint>, ICollectionPointRepository
    {
        public CollectionPointRepository(ParcelantApiV1Context context) : base(context)
        {
        }

        public CollectionPoint GetClientCollectionPoint(int clientId, int collectionPointId)
        {
            return Context.CollectionPoint.Where(x => x.Client.Id == clientId && x.Id == collectionPointId).FirstOrDefault();

        }

        public IEnumerable<CollectionPoint> GetClientCollectionPoints(int clientId)
        {
            return Context.CollectionPoint.Where(x => x.Client.Id == clientId).ToList();
        }
    }
}
