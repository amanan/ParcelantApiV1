﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ParcelantApiV1.Businesslogic.Repository;
using ParcelantApiV1.DAL.Models;

namespace ParcelantApiV1.DAL.Repositoy
{
  public class AppUserRoleRepository : Repository<AppUserRole>, IAppUserRoleRepository
  {
    public AppUserRoleRepository(ParcelantApiV1Context context) : base(context)
    {
    }

    public AppUserRole GetAppUserRoleByCode(int clientId, int appUserId, string code)
    {
            return Context.AppUserRole
                .Include(x => x.AppUser)
                .Include(x => x.AppRole)
                .Join(Context.ClientAppUser,
                  appUserRole => appUserRole.AppUser.Id,
                  clientAppUser => clientAppUser.AppUser.Id,
                  (appUserRole, clientAppUser) => new { AppUserRole = appUserRole, ClientAppUser = clientAppUser }
                  )
                .Where(x => x.ClientAppUser.Client.Id == clientId
                         && x.AppUserRole.AppRole.Code == code
                         && x.AppUserRole.AppUser.Id == appUserId)
                .Select(x => x.AppUserRole)
                .FirstOrDefault();


            /* var query = (from appUserRole in Context.AppUserRole
                         join clientAppUser in Context.ClientAppUser on appUserRole.AppUser.Id equals clientAppUser.AppUser.Id
                         where clientAppUser.Client.Id == clientId && appUserRole.AppRole.Code == code && clientAppUser.AppUser.Id == appUserId
                         select new AppUserRole
                         {
                             AppRole = appUserRole.AppRole,
                             AppUser = clientAppUser.AppUser,
                             CreatedAt = appUserRole.CreatedAt,
                             UpdatedAt = appUserRole.UpdatedAt,
                             Id = appUserRole.Id
                         }).FirstOrDefault();

             return query;*/



        }

    public IEnumerable<AppUserRole> GetAppUsersRoleByCode(int clientid, string roleCode)
    {
       return Context.AppUserRole
        .Include(x => x.AppUser)
        .Include(x => x.AppRole)
        .Join(Context.ClientAppUser,
          appUserRole => appUserRole.AppUser.Id,
          clientAppUser => clientAppUser.AppUser.Id,
          (appUserRole, clientAppUser) => new { AppUserRole = appUserRole, ClientAppUser = clientAppUser }
          )
        .Where(x => x.ClientAppUser.Client.Id == clientid
                 && x.AppUserRole.AppRole.Code == roleCode)
        .Select(x => x.AppUserRole)
        .ToList();
 
    }
  }
}
