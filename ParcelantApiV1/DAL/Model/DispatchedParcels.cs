﻿using ParcelantApiV1.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Model
{
    public class DispatchedParcels
    {
        public int Id { get; set; }
        public DispatchGroup DispatchGroup { get; set; }
        public Parcel  Parcel { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
