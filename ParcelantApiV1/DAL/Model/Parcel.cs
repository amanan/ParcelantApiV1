﻿using ParcelantApiV1.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Models
{
    public class Parcel
    {
        public int Id { get; set; }
        public string ParcelNumber { get; set; }
        public AppUser AppUser { get; set; }
        public Client Client { get; set; }
        public ClientBranch ClientBranch { get; set; }
        public Customer Sender { get; set; }
        public Customer Receiver { get; set; }
        public Routes Routes { get; set; }
        public CollectionPoint CollectionPoint { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

    }

 
    
}
