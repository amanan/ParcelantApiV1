﻿using ParcelantApiV1.Businesslogic;
using ParcelantApiV1.BusinessLogic.Domain;
using ParcelantApiV1.BusinessLogic.Domain.Interfaces;
using ParcelantApiV1.DAL.Model;
using ParcelantApiV1.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL
{
    
    
    public class DispatchManager : IDispatchManager
    {
        private string _prefix = "DGN";
        private IUnitOfWork _unitOfWork;
        public DispatchManager(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void ProcessClientDispatch(Client client, ClientBranch branch,AppUser appUser, Carrier carrier, IEnumerable<int> parcelIdList)
        {
            if (parcelIdList.Count() > 0)
            {
                var dispatchGroup = AddDispatchGroup(client, appUser, carrier);
                if (dispatchGroup != null)
                {
                    foreach (var parcelId in parcelIdList)
                    {
                        var parcel = _unitOfWork.Parcel.GetParcelByClientBranch(client.Id, branch.Id, parcelId);

                        if (parcel != null)
                        {
                            try
                            {
                                var dispatchedParcel = new DispatchedParcels
                                {
                                    DispatchGroup = dispatchGroup,
                                    Parcel = parcel,
                                    CreatedAt = DateTime.Now,
                                    UpdatedAt = DateTime.Now
                                };

                                _unitOfWork.DispatchedParcels.Add(dispatchedParcel);
                                _unitOfWork.Complete();

                                

                            }
                            catch(Exception e)
                            {

                            }
                        }
                    }

                }
            }
        }

        private DispatchGroup AddDispatchGroup(Client client,AppUser appUser, Carrier carrier)
        {
            var groupNumber = MakeClientGroupNumber(client);

            var dispatchGroup = new DispatchGroup
            {
                AppUser = appUser,
                Carrier = carrier,
                Client = client,
                GroupNumber = groupNumber,
                IsClosed = false,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
                
            };

            _unitOfWork.DispatchGroup.Add(dispatchGroup);
            _unitOfWork.Complete();

            return dispatchGroup;
        }
        private string MakeClientGroupNumber(Client client)
        {
            
            var clientDispatchCounter = _unitOfWork.ClientDispatchCounter.Get(client.Id);
            if (clientDispatchCounter == null)
            {
                clientDispatchCounter = new DAL.Model.ClientDispatchCounter
                {
                    Client = client,
                    CurrentCounter = 1,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                };

                _unitOfWork.ClientDispatchCounter.Add(clientDispatchCounter);
                _unitOfWork.Complete();

               

            }
            else
            {
                clientDispatchCounter.CurrentCounter += 1;
                clientDispatchCounter.UpdatedAt = DateTime.Now;
                _unitOfWork.ClientDispatchCounter.Update(clientDispatchCounter);
                _unitOfWork.Complete();




            }
           return _prefix + ZeroPrefix.Handreds(clientDispatchCounter.CurrentCounter);
        }
    }
}
