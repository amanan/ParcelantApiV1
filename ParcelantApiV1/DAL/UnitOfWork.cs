﻿using ParcelantApiV1.Businesslogic;
using ParcelantApiV1.Businesslogic.Repository;
using ParcelantApiV1.BusinessLogic.Repository;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repository;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL
{
    public class UnitOfWork:IUnitOfWork
    {
        private readonly ParcelantApiV1Context _context;

        public UnitOfWork(ParcelantApiV1Context context)
        {
            _context = context;
            ClientBranch = new ClientBranchRepository(_context);
            Clients = new ClientRepository(_context);
            AppRole = new AppRoleRepository(_context);
            AppUser = new AppUserRepository(_context);
            AppUserRole = new AppUserRoleRepository(_context);
            ClientAppUser = new ClientAppUserRepository(_context);
            County = new CountyRepository(_context);
            RouteStop = new RouteStopRepository(_context);
            Route = new RouteRepository(_context);
            ClientBranchRouteStop = new ClientBranchRouteStopRepository(_context);
            Parcel = new ParcelRepository(_context);
            ParcelItem = new ParcelItemRepository(_context);
            ParcelStatus = new ParcelStatusRepository(_context);
            ParcelStatusDetail = new ParcelStatusDetailRepository(_context);
            ParcelItemStatus = new ParcelItemStatusRepository(_context);
            ParcelItemStatusDetail = new ParcelItemStatusDetailRepository(_context);
            ParcelCurrentStatus = new ParcelCurrentStatusRepository(_context);
            ParcelItemCurrentStatus = new ParcelItemCurrentStatusRepository(_context);
            Customer = new CustomerRepository(_context);
            CollectionPoint = new CollectionPointRepository(_context);
            ClientParcelNumberCounter = new ClientParcelNumberCounterRepository(_context);
            ClientDispatchCounter = new ClientDispatchCounterRepository(_context);
            DispatchGroup = new DispatchGroupRepository(_context);
            DispatchedParcels = new DispatchedParcelsRepository(_context);
            Carrier = new CarrierRepository(_context);
        }

      
        public IClientBranchRepository ClientBranch { get; private set; }
        public IClientRepository Clients { get; private set; }
        public IAppUserRepository AppUser { get; private set; }
        public IAppUserRoleRepository AppUserRole { get; private set; }
        public IAppRoleRepository AppRole { get; private set; }
        public IClientAppUserRepository ClientAppUser { get; private set; }
        public ICountyRepository County { get;private set; }

        public IRouteStopRepository RouteStop { get; private set; }
        public IRouteRepository Route { get; set; }
        public IClientBranchRouteStopRepository ClientBranchRouteStop { get; set; }
        public IParcelRepository Parcel { get; set; }
        public IParcelItemRepository ParcelItem { get; set; }
        public IParcelStatusRepository ParcelStatus { get; set; }
        public IParcelStatusDetailRepository ParcelStatusDetail { get; set; }
        public IParcelCurrentStatusRepository ParcelCurrentStatus { get; set; }

        public IParcelItemStatusRepository ParcelItemStatus { get; set; }
        public IParcelItemStatusDetailRepository ParcelItemStatusDetail { get; set; }
        public IParcelItemCurrentStatusRepository ParcelItemCurrentStatus { get; set; }

        public ICustomerRepository Customer { get; set; }
      
        public ICollectionPointRepository CollectionPoint { get; set; }

        public IClientParcelNumberCounterRepository ClientParcelNumberCounter { get; set; }

        public IClientDispatchCounterRepository ClientDispatchCounter { get; set; }
        public IDispatchGroupRepository DispatchGroup { get; set; }
        public IDispatchedParcelsRepository DispatchedParcels { get; set; }

        public ICarrierRepository Carrier { get; set; }


        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
