﻿using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ParcelantApiV1.Util.RestResponse
{
    public class SingleDataResponse<TModel>
    {
        public Boolean Error { get; set; }
        public string ErrorMessage { get; set; }
        public HttpStatusCode Status { get; set; }
        public TModel Data {get; set;}

    }
}
