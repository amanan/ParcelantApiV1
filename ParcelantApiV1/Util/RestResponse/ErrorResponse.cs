﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ParcelantApiV1.Util.RestResponse
{
    public class ErrorResponse
    {
        public Boolean Error { get; set; }
        public string ErrorMessage { get; set; }
        public HttpStatusCode Status { get; set; }
    }
}
