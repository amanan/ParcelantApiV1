﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ParcelantApiV1.Util.RestResponse
{
    public static class Response
    {
        public static IActionResult ListResponse<TModel>(this ListDataResponse<TModel> response)
        {
            var status = HttpStatusCode.OK;

            if (response.Error && response.Status > 0)
            {
                status = response.Status;
            }
            else if (response.Error == false && response.Status > 0)
            {
                status = response.Status;
            }

            return new ObjectResult(response) { StatusCode = (Int32)status };
        }
        public static IActionResult SingleResponse<TModel>(this SingleDataResponse<TModel> response)
        {
            var status = HttpStatusCode.OK;

            if (response.Error && response.Status > 0)
            {
                status = response.Status;
            }
            else if (response.Error == false && response.Status > 0)
            {
                status = response.Status;
            }

            return new ObjectResult(response) { StatusCode = (Int32)status };
        }

    }
}
