﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ParcelantApiV1.Util
{
    public static class PhoneNumberFormatter
    {
        public static string formatPhoneNumber(long unformatednumber)
        {
            var formatednumber = "0";
            var number = unformatednumber.ToString();
            var size = Math.Floor(Math.Log10(unformatednumber) + 1);// unformatednumber;
            if (size <= 10)
            {
                if (number[0] == '0')
                {
                    number = number.Substring(1);
                    formatednumber = "254" + number;

                }
                else
                {
                    formatednumber = "254" + number;
                }
            }
            else if (size == 12 && number[3] != 0)
            {
                formatednumber = number;
            }

            return formatednumber;
        }

        public static bool isPhoneNumber(string number)
        {
            if (number.Length != 9 || number.Length != 10)
            {
                return false;
            }

            if (number.Length == 9)
                {
                    return Regex.Match(number, @"^(\[0-9]{9})$").Success;
                }

            else if (number.Length == 10)
                {
                    return Regex.Match(number, @"^(\[0-9]{10})$").Success;
                }
            else
                {
                    return false;

                }
           
        }
    }
}
