﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ParcelantApiV1.Util
{
    public static class RandomStringGenerator
    {
        public static string generate(int length)
        {
            int maxSize = length;
            int minSize = 5;
            char[] chars = new char[62];
            string a;
            a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            //RNGCryptoServiceProvide crypto = new RNGCryptoServiceProvider();
            RandomNumberGenerator crypto = RandomNumberGenerator.Create();
            crypto.GetBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length - 1)]);

            }
            return result.ToString();
        }
    }
}
